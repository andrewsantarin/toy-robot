function testFunction() {

}

class TestClass {
  constructor() {

  }
}

const valueLookups = [
  {
    name: 'undefined',
    value: undefined,
  },
  {
    name: 'null',
    value: null,
  },
  {
    name: 'boolean',
    value: true,
  },
  {
    name: 'number',
    value: 1,
  },
  {
    name: 'string',
    value: 'a',
  },
  {
    name: 'array',
    value: [],
  },
  {
    name: 'object',
    value: {},
  },
  {
    name: 'function',
    value: () => {},
  },
  {
    name: 'function',
    value: testFunction,
  },
  {
    name: 'Date',
    value: Date,
  },
  {
    name: 'Date()',
    value: new Date(),
  },
  {
    name: 'TestClass',
    value: TestClass
  },
  {
    name: 'TestClass()',
    value: new TestClass(),
  },
];

let commandLookups = [
  {
    commands: [
      'PLACE 0,0,NORTH',
      'MOVE',
      'REPORT',
    ],
    expected: '0,1,NORTH',
  },
  {
    commands: [
      'PLACE 0,0,NORTH',
      'LEFT',
      'REPORT',
    ],
    expected: '0,0,WEST',
  },
  {
    commands: [
      'PLACE 1,2,EAST',
      'MOVE',
      'MOVE',
      'LEFT',
      'MOVE',
      'REPORT',
    ],
    expected: '3,3,NORTH',
  },
];

module.exports = {
  valueLookups,
  commandLookups,
};
