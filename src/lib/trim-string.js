function trimString(str, removeWhitespace = false) {
  // The .replace() function won't remove whitespaces on both ends by itself.
  // The .trim() function does exactly that. Really convenient.
  // https://stackoverflow.com/a/19155648
  return str.trim().replace(/\s+/g, !removeWhitespace ? ' ' : '');
}

module.exports = trimString;
