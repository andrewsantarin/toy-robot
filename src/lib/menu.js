const menu = (choices) => choices.map((choice) => `"${choice}"`).join(' / ');

module.exports = menu;
