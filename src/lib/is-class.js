// https://stackoverflow.com/a/30760236
function isClass(value) {
  if (typeof value !== 'function') {
    return false;
  }

  if (/^\s*class\s+/i.test(value.toString())) {
    return true;
  }

  return false;
}

module.exports = isClass;
