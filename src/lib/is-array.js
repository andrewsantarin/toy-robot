// http://stackoverflow.com/a/16608045/1933636
function isArray(a) {
  return (!!a) && (a.constructor === Array);
};

module.exports = isArray;
