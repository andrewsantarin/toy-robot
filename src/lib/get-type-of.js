import isArray from './is-array';
import isClass from './is-class';
import isDate from './is-date';
import isObject from './is-object';

function getTypeOf(instance) {
  if (instance === null) {
    return 'null';
  }

  const instanceType = typeof instance;

  if (instanceType === 'undefined' || instance.name) {
    if (isClass(instance) || isDate(instance)) {
      return instance.name;
    }

    return instanceType;
  }

  if (instanceType === 'object' && !isObject(instance) && !isArray(instance)) {
    return `${instance.constructor.name}()`;
  }

  return instance.constructor.name.toLowerCase();
}

module.exports = getTypeOf;
