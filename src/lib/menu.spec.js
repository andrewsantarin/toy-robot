import { expect } from 'chai';

import menu from './menu';

describe('menu', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(menu).to.not.be.undefined;
    });

  });

  describe('function', () => {
    it('should generate a string', () => {
      const choices = [
        'Burger',
        'Pasta',
        'Pizza',
        'Lasagna',
      ];
      const expected = '"Burger" / "Pasta" / "Pizza" / "Lasagna"';
      const actual = menu(choices);

      expect(actual).to.equal(expected);
    });
  });
});
