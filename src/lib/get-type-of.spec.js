import { expect } from 'chai';

import { valueLookups } from '../../test/data';
import getTypeOf from './get-type-of';

describe('getTypeOf', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(getTypeOf).to.not.be.undefined;
    });
  });

  describe('function', () => {
    describe('should evaluate correctly types of', () => {
      valueLookups.forEach((valueLookup) => {
        const { name, value } = valueLookup;

        it(name, () => {
          let type = getTypeOf(value);

          expect(type).to.equal(name);
        });
      });
    });
  });
});
