// http://stackoverflow.com/a/16608045/1933636
function isObject(a) {
    return (!!a) && (a.constructor === Object);
};

module.exports = isObject;
