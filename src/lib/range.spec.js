import { expect } from 'chai';

import range from './range';

describe('range', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(range).to.not.be.undefined;
    });
  });

  describe('function', () => {
    describe('should generate an array of values', () => {
      const testCases = [
        {
          input: {
            start: 0,
            stop: 5,
          },
          expected: [ 0, 1, 2, 3, 4 ],
        },
        {
          input: {
            start: 1,
            stop: 8,
            step: 2,
          },
          expected: [ 1, 3, 5 ],
        },
        {
          input: {
            start: null,
            stop: 4,
          },
          expected: [0, 1, 2, 3],
        },
        {
          input: {
            start: false,
            stop: 4,
          },
          expected: [0, 1, 2, 3],
        },
      ];

      testCases.forEach((testCase) => {
        const { input, expected } = testCase;
        const { start, stop, step } = input;
        const testCaseName = [
          `start at ${start}`,
          `stop at ${stop}`,
          !!step ? `increment at ${step}` : 'no increment given',
        ].join(', ');

        it(testCaseName, () => {
          const actual = range(start, stop, step);

          expect(actual).to.deep.equal(expected);
        });
      });
    });

    describe('should generate an empty array of values', () => {
      const expected = [];

      const testCases = [
        {
          input: {
            start: null,
            stop: null,
          },
          expected,
        },
        {
          input: {
            start: undefined,
            stop: undefined,
            step: 2,
          },
          expected,
        },
        {
          input: {
            start: 0,
          },
          expected,
        },
        {
          input: {
            start: 5,
          },
          expected,
        },
        {
          input: {
          },
          expected,
        },
        {
          input: {
            start: 3,
            stop: 1,
            step: 2,
          },
          expected,
        },
      ];

      testCases.forEach((testCase) => {
        const { input, expected } = testCase;
        const { start, stop, step } = input;
        const testCaseName = [
          `start at ${start}`,
          `stop at ${stop}`,
          !!step ? `increment at ${step}` : 'no increment given',
        ].join(', ');

        it(testCaseName, () => {
          const actual = range(start, stop, step);

          expect(actual).to.deep.equal(expected);
        });
      });
    });
  });
});
