// https://stackoverflow.com/a/30760236
function isDate(value) {
  if (typeof value !== 'function') {
    return false;
  }

  // Actually inspired by this regex:
  // https://stackoverflow.com/a/15472787
  if (/^\s*function( Date+)+/i.test(value.toString())) {
    return true;
  }

  return false;
}

module.exports = isDate;
