import { expect } from 'chai';

import trimString from './trim-string';

describe('trimString', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(trimString).to.not.be.undefined;
    });
  });

  describe('function', () => {
    it('should trim all whitespace', () => {
      const str = '                There   are       too      many  whitespaces      ';
      const actual = trimString(str);
      const expected = 'There are too many whitespaces';

      expect(actual.length).to.equal(expected.length);
      expect(actual).to.equal(expected);
    });
  });
});
