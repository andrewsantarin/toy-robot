import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { expect } from 'chai';

import App from './App';

describe('App', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(App).to.not.be.undefined;
    });
  });
});
