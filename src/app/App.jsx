import React, { Component } from 'react';

import trimString from '../lib/trim-string';

import TabletopModel from './tabletop/tabletop.model';
import TabletopGrid from './tabletop/TabletopGrid';

import RobotModel from './robot/robot.model';
import RobotCommandInput from './robot/RobotCommandInput';
import RobotCommandManual from './robot/RobotCommandManual';
import RobotCommandLog from './robot/RobotCommandLog';

const TABLETOP = {
  width: 5,
  height: 5,
};

const tabletop = new TabletopModel(TABLETOP);
const robot = new RobotModel(tabletop.coordinatesInBounds.bind(tabletop));

class App extends Component {
  state = {
    commands: [],
    status: '',
  };

  static propTypes = {

  };

  static defaultProps = {

  };

  tabletop = tabletop;
  robot = robot;

  setCommandsState = (commands, callback) => {
    this.setState((prevState, props) => ({
      commands,
    }), callback);
  }

  setStatusState = (status, callback) => {
    this.setState((prevState, props) => ({
      status,
    }), callback);
  }

  logCommand = (command) => {
    const commands = [
      ...this.state.commands,
      command,
    ];
    
    this.setCommandsState(commands);
  }

  readCommand = (command) => {
    try {
      const result = this.robot.readCommand(command);
      this.setStatusState(result || '');
    } catch (error) {
      this.setStatusState(error.message);
    }
  }

  handleRobotCommandInputSubmit = (value) => {
    this.readCommand(value);
    this.logCommand(value);
  }

  render() {
    const { dimensions } = this.props;
    const { commands, status } = this.state;

    return (
      <box
        top="center"
        left="center"
        width="100%"
        height="100%"
      >
        <TabletopGrid
          tabletop={this.tabletop}
          marker={this.robot.props}
          dimensions={dimensions}
        />
        <RobotCommandInput
          robot={this.robot}
          onRobotCommandInputSubmit={this.handleRobotCommandInputSubmit}
          status={status}
        />
        <RobotCommandManual />
        <RobotCommandLog
          commands={commands}
          order="desc"
          size={10}
          title="Robot Input History"
        />
      </box>
    );
  }
}

module.exports = App;
