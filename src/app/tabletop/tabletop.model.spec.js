import { expect } from 'chai';

import TabletopModel from './tabletop.model';

describe('TabletopModel', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(TabletopModel).to.not.be.undefined;
    });
  });
});
