import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TabletopGrid extends Component {
  icons = {
    N: '^',
    E: '>',
    S: 'v',
    W: '<',
  };

  renderCell = ({ x, y }) => {
    if (x === -1 && y === -1) {
      return '  ';
    }
    if (y === -1) {
      return ` ${x} `;
    }

    if (x === -1) {
      return ` ${y} `;
    }

    const { marker = {} } = this.props;
    const { coordinates = {}, facing } = marker;
    const icon = this.icons[facing];

    if (x === coordinates.x && y === coordinates.y && !!icon) {
      return ` ${this.icons[facing]} `;
    }

    return ' _ ';
  }

  addYRow = (cells) => {
    let newCells = cells;
    newCells.push(
      cells[cells.length - 1].map((cell) => ({ x: cell.x, y: -1 }))
    );

    return newCells;
  }

  render() {
    const { tabletop, marker } = this.props;
    const { matrix } = tabletop.props;

    // Computers: top...bottom, left...right.
    // Space: bottom...top, left...right.
    // Reverse y-axis only to get the Space result.
    const spacing = 3;
    const space = [ ...matrix ].reverse();
    space.push(space[space.length - 1].map((cell) => ({ x: cell.x, y: -1 })));

    return (
      <element>
        <box
          top="4%"
          left="8%"
          height={1}
        >
          [ Robot Grid Positioning System ]
        </box>
        <box
          width="100%"
          height="20%"
          top="7%"
          left="11%"
        >
          {space.map((cells, yIndex) => (
            <box
              key={yIndex}
              top={yIndex}
              width={space.length * spacing}
              height={1}
            >
              {[ { x: -1, y: cells[0].y }, ...cells ].map((cell, xIndex) => (
                <box
                  key={xIndex}
                  left={xIndex * spacing}
                  width={spacing}
                  height={1}
                >
                  {this.renderCell(cell)}
                </box>
              ))}
            </box>
          ))}
        </box>
      </element>
    );
  }
}

module.exports = TabletopGrid;
