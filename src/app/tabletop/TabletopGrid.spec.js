import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { expect } from 'chai';

import TabletopGrid from './TabletopGrid';

describe('TabletopGrid', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(TabletopGrid).to.not.be.undefined;
    });
  });
});
