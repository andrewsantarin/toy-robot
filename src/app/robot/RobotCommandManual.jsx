import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RobotCommandManual extends Component {
  static propTypes = {

  };

  static defaultProps = {

  };

  render() {
    const CONTENT = [
      'TOY ROBOT SIMULATOR COMMAND-LINE INTERFACE',
      '',
      'CONTROLLING THE ROBOT',
      '=====================',
      'You can issue the following commands on the "Enter Command" text input:',
      '  PLACE x,y,f : Puts the robot down on the tabletop at a specific point (x = 0-9, y = 0-9, f = NORTH,EAST,SOUTH,WEST)',
      '  MOVE        : Moves the robot one step forward in the direction it is facing',
      '  LEFT        : Turns the robot anticlockwise, changing direction. e.g. left of NORTH is WEST',
      '  RIGHT       : Turns the robot clockwise, changing direction. e.g. right of NORTH is EAST',
      '  REPORT      : Shows the robot\'s current position, if it has been placed. e.g. 1,2,SOUTH',
      '',
      'A 5x5 grid is shown above the text input to help you visualize where the robot is going.',
      '',
      '',
      'KEYBOARD CONTROLS',
      '=================',
      '  [ENTER]    - submits the current command',
      '  [ESC]      - leave the input, which then allows you to leave the terminal. See available keys below.',
      '  [CTRL + C] - leave the terminal',
      '  [ESC]      - leave the terminal',
      '  [Q]        - leave the terminal',
    ];

    return (
      <box
        height="50%"
        top="20%"
        left="50%"
        class={{
          border: {
            type: 'line',
          },
          style: {
            border: {
              fg: 'blue',
            },
          },
        }}
      >
        {(CONTENT).map((text, index) => (
          <box top={index} height={1}>
            {text}
          </box>
        ))}
      </box>
    );
  }
}

module.exports = RobotCommandManual;
