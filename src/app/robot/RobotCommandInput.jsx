import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RobotCommandInput extends Component {
  state = {
    value: '',
    hint: true,
    mounted: false,
  };

  static propTypes = {

  };

  static defaultProps = {

  };

  componentDidMount() {
    this.refs.textbox.focus();
  }

  setHintState = (hint, callback) => {
  }

  setValueState = (value, callback) => {
    this.setState((prevState, props) => ({
      value,
    }), callback);
  }

  handleSubmit = (value) => {
    this.props.onRobotCommandInputSubmit(value);
    this.refs.textbox.clearValue();
    this.setValueState(value, () => {
      this.refs.textbox.focus();
    });
  }

  handleFocus = () => {
  }

  handleReset = () => {
    this.setValueState('');
  }

  handleCancel = () => {
    this.setValueState('');
  }

  render() {
    const { status } = this.props;
    const { value } = this.state;

    return (
      <form
        keys
        vi
        focused
        onSubmit={this.handleSubmit}
        onReset={this.handleReset}
        top="20%"
        width="30%"
        height={12}
      >
        <box
          width={25}
          height={3}
          class={{
            border: {
              type: 'line',
            },
            style: {
              border: {
                fg: 'black',
              },
            },
          }}
        >
          Enter Command:
        </box>
        <textbox
          ref="textbox"
          onFocus={this.handleFocus}
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          left={15}
          height={3}
          keys
          mouse
          inputOnFocus
          class={{
            border: {
              type: 'line',
            },
            style: {
              border: {
                fg: 'blue',
              },
            },
          }}
        />
        <box
          width={18}
          height={3}
          top="20%"
          left="70%"
        >
          [ESC]: leave input
        </box>
        <box
          label="Robot Says..."
          top={3}
          height={7}
          width="100%"
          class={{
            border: {
              type: 'line',
            },
            style: {
              border: {
                fg: 'blue',
              },
            },
          }}
        >
          {status}
        </box>
      </form>
    );
  }
}

module.exports = RobotCommandInput;
