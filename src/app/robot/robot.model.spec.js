import { expect } from 'chai';

import trimString from '../../lib/trim-string';

import { coordinatesToString } from '../geometry/point/point.utils';
import SpaceModel from '../geometry/space/space.model';
import RobotModel from './robot.model';

const SPACE = {
  dimensions: {
    width: 3,
    height: 4,
  },
};

const space = new SpaceModel(SPACE.dimensions);

const ORIGIN = {
  coordinates: {
    x: 0,
    y: 0,
  },
};

const ROBOT = {
  coordinates: {
    x: SPACE.dimensions.width - 1,
    y: SPACE.dimensions.height - 1,
  },
  facing: 'N',
};

const INVALID_ROBOT = {
  coordinates: Object.keys(ROBOT.coordinates).reduce((lookup, key) => ({
    ...lookup,
    [key]: ROBOT.coordinates[key] + 1,
  }), {}),
  facing: 'X',
};

describe('RobotModel', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(RobotModel).to.not.be.undefined;
    });
  });

  describe('dependency', () => {
    describe('trimString', () => {
      describe('module', () => {
        it('should exist', () => {
          expect(trimString).to.not.be.undefined;
        });
      });
    });
  });

  describe('class', () => {
    describe('constructor', () => {
      it('should initialize with an external coordinate check function', () => {
        function coordinateChecker() {
          return true;
        }

        const robot = new RobotModel(coordinateChecker);

        expect(robot).to.have.property('_coordinatesAreValid');
        expect(robot.coordinatesAreValid()).to.equal(coordinateChecker());
      });
    });

    describe('function::static', () => {
      describe('createCommand', () => {
        describe('falsy values', () => {
          it('should throw an error', () => {
            const message = 'I\'m sorry. I don\'t understand that kind of command.';
            const data = {
              str: undefined,
            };

            function createCommand() {
              RobotModel.createCommand();
            }

            expect(createCommand).to.throw(Error).with.property('message', message);
            expect(createCommand).to.throw(Error).with.deep.own.property('data', data);
          });
        });

        describe('non-falsy values', () => {
          it('should trim all whitespace', () => {
            const str = '                There   are       too      many  whitespaces      ';
            const actual = RobotModel.createCommand(str);
            const expected = 'There are too many whitespaces';

            expect(actual.length).to.equal(expected.length);
            expect(actual.length).to.equal(trimString(str).length);
          });

          it('should uppercase all lowercase', () => {
            const str = 'This will be eventually Uppercased';
            const actual = RobotModel.createCommand(str);
            const expected = 'THIS WILL BE EVENTUALLY UPPERCASED';

            expect(actual).to.equal(expected);
            expect(actual).to.equal(str.toUpperCase());
          });

          it('should trim all whitespace & uppercase all lowercase', () => {
            const str = '      Whitespace    go  away, PLEASE!   Trim!    ';
            const actual = RobotModel.createCommand(str);
            const expected = 'WHITESPACE GO AWAY, PLEASE! TRIM!';

            expect(actual.length).to.equal(expected.length);
            expect(actual.length).to.equal(trimString(str).length);
            expect(actual).to.equal(expected);
            expect(actual).to.equal(trimString(str).toUpperCase());
          });
        });

        describe('forced throw', () => {
          const VALID_COMMANDS = [
            'PLACE 1,2,N',
            'LEFT',
            'RIGHT',
            'REPORT',
            'UNKNOWN COMMAND',
          ];

          (VALID_COMMANDS).forEach((command) => {
            it(`should always throw an error even if valid: "${command}"`, () => {
              const message = 'I\'m sorry. I don\'t understand that kind of command.';
              const data = {
                str: command,
              };

              function createCommand() {
                RobotModel.createCommand(command, true);
              }

              expect(createCommand).to.throw(Error).with.property('message', message);
              expect(createCommand).to.throw(Error).with.deep.own.property('data', data);
            });
          })
        });
      });
    });

    describe('function::instance', () => {
      describe('readCommand', () => {
        function getReport(coordinates, facing) {
          return `${coordinates.x},${coordinates.y},${RobotModel.movementVectors[facing].name.toUpperCase()}`;
        }

        describe('should throw errors when given unexpected commands', () => {
          const robot = new RobotModel(space.coordinatesInBounds.bind(space));

          const INVALID_COMMANDS = [
            'SKFJSLDFJKLJ ',
            'DKFJO _!E E',
            293298,
            'IEOIE ',
            new Date(),
          ];

          (INVALID_COMMANDS).forEach((command) => {
            it(`invalidate "${command}"`, () => {
              const message = 'I\'m sorry. I don\'t understand that kind of command.';
              const data = {
                str: command,
              };

              function readCommand() {
                robot.readCommand(command);
              }

              expect(readCommand).to.throw(Error).with.property('message', message);
              expect(readCommand).to.throw(Error).with.deep.own.property('data', data);
            })
          });
        });

        describe('should read commands', () => {
          describe('"REPORT"', () => {
            const robot = new RobotModel(space.coordinatesInBounds.bind(space));
            const command = 'REPORT';

            it('should generate report when coordinates and facing are valid', () => {
              robot.place(ROBOT.coordinates, ROBOT.facing);

              const expected = getReport(robot.coordinates, robot.facing);

              function readCommand() {
                const report = robot.readCommand(command);

                return report;
              }

              expect(readCommand).to.not.throw(Error);
              expect(readCommand()).to.equal(expected);
            });

            const MISSING_COORDINATE_LOOKUPS = [
              INVALID_ROBOT.coordinates,
              { x: ROBOT.coordinates.x },
              { y: ROBOT.coordinates.y },
            ];

            (MISSING_COORDINATE_LOOKUPS).forEach((coordinates) => {
              it(`should not generate report when coordinates are invalid ${coordinatesToString(coordinates)}`, () => {
                robot.place(coordinates, ROBOT.facing, true);

                const message = `I'm sorry. I can't report yet. Please place me somewhere appropriate first.`;

                function readCommand() {
                  const report = robot.readCommand(command);

                  return report;
                }

                expect(readCommand).to.throw(Error).with.property('message', message);
              });
            });

            it('should not generate report when facing is invalid', () => {
              robot.place(ROBOT.coordinates, INVALID_ROBOT.facing, true);

              const message = `I'm sorry. I can't report yet. Please place me somewhere appropriate first.`;

              function readCommand() {
                const report = robot.readCommand(command);

                return report;
              }

              expect(readCommand).to.throw(Error).with.property('message', message);
            });
          });

          describe('"PLACE"', () => {
            const robot = new RobotModel(space.coordinatesInBounds.bind(space));
            const command = 'PLACE';
            const direction = 'NORTH';

            describe('should read valid arguments', () => {
              const dirtyCommand = `    ${command}     ${ROBOT.coordinates.x}, ${ROBOT.coordinates.y}  ,   ${direction}`;

              it(`with excess spaces: "${dirtyCommand}"`, () => {
                function readCommand() {
                  robot.readCommand(dirtyCommand);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.deep.equal(ROBOT.coordinates);
                expect(robot.facing).to.equal(ROBOT.facing);
              });

              const cleanCommand = `${command} ${trimString(dirtyCommand.replace(command, ''), true)}`;

              it(`without excess spaces: "${cleanCommand}"`, () => {
                function readCommand() {
                  robot.readCommand(dirtyCommand);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.deep.equal(ROBOT.coordinates);
                expect(robot.facing).to.equal(ROBOT.facing);
              });
            });

            describe('should read invalid arguments', () => {
              it(`with invalid coordinates`, () => {
                function readCommand() {
                  robot.readCommand(`${command} ${INVALID_ROBOT.coordinates.x},${INVALID_ROBOT.coordinates.y},${direction}`);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.deep.equal(INVALID_ROBOT.coordinates);
                expect(robot.facing).to.not.be.undefined;
              });

              it(`with invalid facing`, () => {
                function readCommand() {
                  robot.readCommand(`${command} ${ROBOT.coordinates.x},${ROBOT.coordinates.y}`);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.deep.equal(ROBOT.coordinates);
                expect(robot.facing).to.be.undefined;
              });

              it(`with short-form facing (interpreted as "null")`, () => {
                function readCommand() {
                  robot.readCommand(`${command} ${ROBOT.coordinates.x},${ROBOT.coordinates.y},${direction.charAt(0)}`);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.deep.equal(ROBOT.coordinates);
                expect(robot.facing).to.be.null;
              });

              it('with no arguments provided', () => {
                function readCommand() {
                  robot.readCommand(command);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.coordinates).to.be.undefined;
                expect(robot.facing).to.be.undefined;
              });
            });
          });

          describe('"MOVE"', () => {
            const command = 'MOVE';

            describe(`at corner coordinate ${coordinatesToString(ROBOT.coordinates)}`, () => {
              describe('invalid movement', () => {
                const robot = new RobotModel(space.coordinatesInBounds.bind(space));

                it('should not set coordinates when not placed', () => {
                  const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

                  function readCommand() {
                    robot.readCommand(command);
                  }

                  expect(readCommand).to.throw(Error).with.property('message', message);
                });

                it('should not set coordinates when placed with invalid facing', () => {
                  robot.place(ROBOT.coordinates, INVALID_ROBOT.facing, true);

                  const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

                  function readCommand() {
                    robot.readCommand(command);
                  }

                  expect(readCommand).to.throw(Error).with.property('message', message);
                });

                const MISSING_COORDINATE_LOOKUPS = [
                  INVALID_ROBOT.coordinates,
                  { x: ROBOT.coordinates.x },
                  { y: ROBOT.coordinates.y },
                ];

                (MISSING_COORDINATE_LOOKUPS).forEach((coordinates) => {
                  it(`should not set coordinates when placed with invalid coordinates at ${coordinatesToString(coordinates)}`, () => {
                    robot.place(coordinates, ROBOT.facing, true);

                    const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

                    function readCommand() {
                      robot.readCommand(command);
                    }

                    expect(readCommand).to.throw(Error).with.property('message', message);
                  });
                });
              });

              describe('valid movement', () => {
                const robot = new RobotModel(space.coordinatesInBounds.bind(space));
                const DIRECTIONS = ['N', 'E', 'S', 'W'];

                it('should have 4 directions arranged clockwise starting from "N"', () => {
                  expect(DIRECTIONS).to.deep.equal(Object.keys(RobotModel.movementVectors));
                });

                const caseName = [
                  'should move 1 unit in every direction',
                  'in a clockwise circle starting from "N"',
                  'using .place() instead of .turn(),',
                  `returning back to ${coordinatesToString(ORIGIN.coordinates)}`,
                ].join(' ');

                describe(caseName, () => {
                  let c = ORIGIN.coordinates;

                  const COORDINATES_LOOKUPS = (DIRECTIONS).map((direction) => {
                    c = Object.keys(c).reduce((lookup, key) => ({
                      ...lookup,
                      [key]: c[key] + RobotModel.movementVectors[direction][key],
                    }), {});

                    return {
                      coordinates: c,
                      facing: direction,
                    };
                  });

                  // Reuse.
                  c = ORIGIN.coordinates;

                  (COORDINATES_LOOKUPS).forEach((lookup) => {
                    const { coordinates, facing } = lookup;
                    const movementVector = RobotModel.movementVectors[facing];

                    it(`set coordinates to ${coordinatesToString(coordinates)}, facing: "${facing}"`, () => {
                      expect(true).to.not.be.undefined;

                      robot.place(c, facing);

                      function readCommand() {
                        robot.readCommand(command);
                      }

                      expect(readCommand).to.not.throw(Error);
                      expect(robot.coordinates).to.deep.equal(coordinates);
                      expect(robot.facing).to.equal(facing);

                      c = Object.keys(c).reduce((lookup, key) => ({
                        ...lookup,
                        [key]: c[key] + movementVector[key],
                      }), {});
                    });
                  });
                });
              });
            });

            const COORDINATES_LOOKUPS = [
              {
                coordinates: ORIGIN.coordinates,
                name: 'Origin',
                directions: {
                  valid: [ 'N', 'E' ],
                  invalid: [ 'S', 'W' ],
                },
              },
              {
                coordinates: ROBOT.coordinates,
                name: 'Robot',
                directions: {
                  valid: [ 'S', 'W' ],
                  invalid: [ 'N', 'E' ],
                },
              },
            ];

            (COORDINATES_LOOKUPS).forEach(({ coordinates, name, directions }) => {
              describe(`at coordinate ${name} ${coordinatesToString(coordinates)}`, () => {
                const robot = new RobotModel(space.coordinatesInBounds.bind(space));

                function caseName(coordinates, facing, positive = true) {
                  return `should be ${positive ? 'able' : 'unable'} to move "${facing}" to ${coordinatesToString(coordinates)}`;
                }

                describe('valid movement', () => {
                  directions.valid.forEach((facing) => {
                    const movementVector = RobotModel.movementVectors[facing];
                    const expectedCoordinates = Object.keys(coordinates).reduce((lookup, key) => ({
                      ...lookup,
                      [key]: coordinates[key] + movementVector[key],
                    }), {});

                    it(caseName(expectedCoordinates, facing), () => {
                      robot.place(coordinates, facing);

                      function readCommand() {
                        robot.readCommand(command);
                      }

                      expect(readCommand).to.not.throw(Error);
                      expect(robot.coordinates).to.deep.equal(expectedCoordinates);
                      expect(robot.facing).to.equal(facing);
                    });
                  });
                });

                describe('invalid movement', () => {
                  directions.invalid.forEach((facing) => {
                    const movementVector = RobotModel.movementVectors[facing];
                    const expectedCoordinates = Object.keys(coordinates).reduce((lookup, key) => ({
                      ...lookup,
                      [key]: coordinates[key] + movementVector[key],
                    }), {});

                    it(caseName(expectedCoordinates, facing, false), () => {
                      robot.place(coordinates, facing);

                      const vector = facing ? 'be placed at' : `go ${movementVector.name} to`;
                      const message = `I'm sorry. I can't ${vector}: (${expectedCoordinates.x},${expectedCoordinates.y}). I'll fall off.`;
                      const data = {
                        coordinates: expectedCoordinates,
                        facing,
                      };

                      function readCommand() {
                        robot.readCommand(command);
                      }

                      expect(readCommand).to.throw(Error).with.property('message', message);
                      expect(readCommand).to.throw(Error).with.own.deep.property('data', data);
                    });
                  });
                });
              });
            });
          });

          const DIRECTION_COMMANDS = ['LEFT', 'RIGHT'];

          (DIRECTION_COMMANDS).forEach((command) => {
            describe(`"${command}"`, () => {
              const robot = new RobotModel(space.coordinatesInBounds.bind(space));

              it('should not be able to rotate when not placed', () => {
                const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

                function readCommand() {
                  robot.readCommand(command);
                }

                expect(readCommand).to.throw(Error).with.property('message', message);
              });

              it('should be able to rotate if the placement is valid', () => {
                robot.place(ROBOT.coordinates, ROBOT.facing);

                function readCommand() {
                  robot.readCommand(command);
                }

                expect(readCommand).to.not.throw(Error);
                expect(robot.facing).to.not.equal(ROBOT.facing);
              });

              it('should not be able to rotate if the placement is has invalid coordinates', () => {
                robot.place(INVALID_ROBOT.coordinates, ROBOT.facing, true);

                const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

                function readCommand() {
                  robot.readCommand(command);
                }

                expect(readCommand).to.throw(Error).with.property('message', message);
              });

              it('should not be able to rotate if the placement is has invalid facing', () => {
                robot.place(ROBOT.coordinates, INVALID_ROBOT.facing, true);

                const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

                function readCommand() {
                  robot.readCommand(command);
                }

                expect(readCommand).to.throw(Error).with.property('message', message);
              });
            });
          });
        });
      });
    });
  });
});
