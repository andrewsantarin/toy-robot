import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { expect } from 'chai';

import RobotCommandInput from './RobotCommandInput';

describe('RobotCommandInput', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(RobotCommandInput).to.not.be.undefined;
    });
  });
});
