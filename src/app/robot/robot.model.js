import getTypeOf from '../../lib/get-type-of';
import trimString from '../../lib/trim-string';

import PointModel from '../geometry/point/point.model';

class RobotModel extends PointModel {
  static createCommand(str, forceThrowError = false) {
    if (!str || getTypeOf(str) !== 'string' || !!forceThrowError) {
      const message = 'I\'m sorry. I don\'t understand that kind of command.';
      const data = {
        str,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    // Trim the string of whitespace, uppercase the entire string.
    const command = trimString(str).toUpperCase();

    return command;
  }

  readCommand(str) {
    // Clean the command string, then retrieve the first word.
    // https://stackoverflow.com/a/46999443
    const command = RobotModel.createCommand(str);
    const action = command.replace(/ .*/,'');

    switch (action) {
      case 'PLACE': {
        // When extracting the arguments, avoid possibilities of getting this: ['']. It screws up logic.
        // Translate [''] into [] to prevent code from breaking.
        const argsStr = trimString(command.replace(`${action}`, ''), true);
        const args = argsStr.length > 0 ? trimString(command.replace(`${action}`, ''), true).split(',') : [];

        const [ rawX, rawY, rawFacing ] = args;
        const rawCoordinates = {
          x: rawX,
          y: rawY,
        };
        const coordinatesExist = getTypeOf(rawX) !== 'undefined' && getTypeOf(rawY) !== 'undefined';
        const coordinates = !coordinatesExist ? undefined : Object.keys(rawCoordinates).reduce((lookup, key) => {
          if (getTypeOf(rawCoordinates[key]) === 'undefined') {
            return lookup;
          }

          return {
            ...lookup,
            [key]: !isNaN(rawCoordinates[key]) ? parseInt(rawCoordinates[key]) : rawCoordinates[key],
          };
        }, {});

        let facing = rawFacing;
        const fullFacings = Object.keys(RobotModel.movementVectors).map((key) => (
          RobotModel.movementVectors[key].name.toUpperCase()
        ));
        const facingValues = Object.keys(RobotModel.movementVectors);

        // The facing must be written in full in order to be processed.
        // Nullify the value if it's just one character long because that's how the model reads it.
        if (fullFacings.indexOf(facing) >= 0) {
          facing = rawFacing.toUpperCase().charAt(0);
        } else if (facingValues.indexOf(facing) >= 0) {
          facing = null;
        }

        // It's expected that if the placement was invalid, any further commands would be ignored.
        // Such a scenario needs the instance to avoid validating itself before storing the props.
        // Thus, the instance must memorize the bad placement for the rest of its life until placed correctly.
        const loose = true;

        this.place(coordinates, facing, loose);
        return;
      }

      case 'MOVE':
        // Simple call.
        this.move();
        return;

      case 'LEFT':
      case 'RIGHT':
        // No fancy reverse key-value reading here. The first letter of the action is good enough.
        this.turn(action.charAt(0));
        return;

      case 'REPORT':
        // Return the result of the report generator.
        return this.report();

      default:
        // Reuse the static utility function to throw an error.
        RobotModel.createCommand(str, true);
    }
  }
}

module.exports = RobotModel;
