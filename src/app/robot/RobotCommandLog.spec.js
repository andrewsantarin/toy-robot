import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { expect } from 'chai';

import RobotCommandLog from './RobotCommandLog';

describe('RobotCommandLog', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(RobotCommandLog).to.not.be.undefined;
    });
  });
});
