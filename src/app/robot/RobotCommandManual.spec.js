import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { expect } from 'chai';

import RobotCommandManual from './RobotCommandManual';

describe('RobotCommandManual', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(RobotCommandManual).to.not.be.undefined;
    });
  });
});
