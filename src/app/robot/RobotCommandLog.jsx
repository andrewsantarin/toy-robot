import React, { Component } from 'react';
import PropTypes from 'prop-types';

export const PROP_TYPES = {
  
};

export const DEFAULT_PROPS = {
  
};

export default class RobotCommandLog extends Component {
  getCommands() {
    const { commands, order } = this.props;

    if (order === 'desc') {
      return [ ...commands ].reverse();
    }

    return commands;
  }

  render() {
    const { size, title } = this.props;
    const commands = this.getCommands();

    return (
      <box
        label={title}
        top="40%"
        width="30%"
        class={{
          border: {
            type: 'line',
          },
          style: {
            border: {
              fg: 'blue',
            },
          },
        }}
      >
        {commands.slice(0, size).map((command, index) => (
          <box height={1} width={command.length} top={index} key={`${index}-${command}`}>
            {command}
          </box>
        ))}
      </box>
    );
  }
}

RobotCommandLog.propTypes    = PROP_TYPES;
RobotCommandLog.defaultProps = DEFAULT_PROPS;
