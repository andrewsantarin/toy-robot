import { expect } from 'chai';

import * as SpaceUtils from './space.utils';

describe('SpaceUtils', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(SpaceUtils).to.not.be.undefined;
    });
  });
});
