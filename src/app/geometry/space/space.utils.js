function dimensionsToString({ width, height }) {
  return `${width} × ${height}`;
}

module.exports = {
  dimensionsToString
};
