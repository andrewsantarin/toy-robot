import range from '../../../lib/range';

class SpaceModel {
  constructor(dimensions) {
    this.matrix = dimensions;
  }

  get matrix() {
    return this._matrix;
  }

  set matrix(dimensions = {}) {
    this._matrix = SpaceModel.createMatrix(dimensions);
  }

  static createMatrix(dimensions = {}) {
    const { width, height } = dimensions;

    if (!width || !height) {
      const message = 'The space won\'t work with no width or no height.';
      const data = {
        width,
        height,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    return range(0, height).map((y) => range(0, width).map((x) => ({ x, y })));
  }

  coordinatesInBounds(coordinates = {}) {
    const { x, y } = coordinates;

    return !!this._matrix[y] && !!this._matrix[y][x];
  }

  get props() {
    const matrix = this.matrix;

    return {
      matrix,
    };
  }

  report() {
    const { matrix } = this.props;

    const matrixStr = matrix.map((points) => points.map((point) => `${point.x},${point.y}`).join('  ')).join('\n\n');

    return matrixStr;
  }
}

module.exports = SpaceModel;
