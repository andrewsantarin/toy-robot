import { expect } from 'chai';

import { coordinatesToString } from '../point/point.utils';
import { dimensionsToString } from './space.utils';

import SpaceModel from './space.model';

const COORDINATES_LABEL = coordinatesToString({
  x: 'x',
  y: 'y',
});

const SPACE = {
  dimensions: {
    width: 3,
    height: 4,
  },
};

describe('SpaceModel', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(SpaceModel).to.not.be.undefined;
    });
  });

  describe('class', () => {
    describe('constructor', () => {
      it('should instantiate when initialized with valid SPACE.dimensions', () => {
        const space = new SpaceModel(SPACE.dimensions);

        expect(space.matrix.length).to.equal(SPACE.dimensions.height);
        expect(space.matrix[0].length).to.equal(SPACE.dimensions.width);
      });

      it('should not instantiate when initialized with invalid SPACE.dimensions', () => {
        const message = 'The space won\'t work with no width or no height.';
        const data = {
          width: undefined,
          height: undefined,
        };

        function createSpaceModel() {
          return new SpaceModel();
        }

        expect(createSpaceModel).to.throw(Error).with.property('message', message);
        expect(createSpaceModel).to.throw(Error).with.deep.own.property('data', data);
      });
    });

    describe('function::static', () => {
      describe('createMatrix', () => {
        it('should create a matrix with valid SPACE.dimensions', () => {
          const matrix = SpaceModel.createMatrix(SPACE.dimensions);

          expect(matrix.length).to.equal(SPACE.dimensions.height);
          expect(matrix[0].length).to.equal(SPACE.dimensions.width);
        });

        it('should not create a matrix with invalid SPACE.dimensions', () => {
          const message = 'The space won\'t work with no width or no height.';
          const data = {
            width: undefined,
            height: undefined,
          };

          expect(SpaceModel.createMatrix).to.throw(Error).with.property('message', message);
          expect(SpaceModel.createMatrix).to.throw(Error).with.deep.own.property('data', data);
        });
      });
    });

    describe('function::instance' , () => {
      describe('coordinatesInBounds', () => {
        describe(`on Space (${dimensionsToString(SPACE.dimensions)})`, () => {
          const firstX = 0;
          const firstY = 0;
          const lastX = SPACE.dimensions.width - 1;
          const lastY = SPACE.dimensions.height - 1;

          const corners = [
            { // Southwest
              x: firstX,
              y: firstY,
            },
            { // Southeast
              x: lastX,
              y: firstY,
            },
            { // Northwest
              x: firstX,
              y: lastY,
            },
            { // Northeast
              x: lastX,
              y: lastY,
            },
          ];

          const space = new SpaceModel(SPACE.dimensions);

          function calcOutOfBoundsOffset(coordinate) {
            const offset = coordinate > 0 ? +1 : -1;

            return offset;
          }

          describe(`should evaluate ${COORDINATES_LABEL} as: valid`, () => {
            const expected = true;

            corners.forEach((corner) => {
              it(coordinatesToString(corner), () => {
                const actual = space.coordinatesInBounds(corner);

                expect(actual).to.equal(expected);
              });
            });
          });

          describe(`should evaluate ${COORDINATES_LABEL} as: invalid`, () => {
            const expected = false;

            [['x'], ['y'], ['x', 'y']].forEach((axes) => {
              corners.forEach((corner) => {
                const coordinate = Object.keys(corner).reduce((lookup, key) => ({
                  ...lookup,
                  [key]: corner[key] + (axes.indexOf(key) >= 0 ? calcOutOfBoundsOffset(corner[key]) : 0),
                }), {});

                it(coordinatesToString(coordinate), () => {
                  const actual = space.coordinatesInBounds(coordinate);

                  expect(actual).to.equal(expected);
                });
              });
            });
          });
        });
      });
    });
  });
});
