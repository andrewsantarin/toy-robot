import getTypeOf from '../../../lib/get-type-of';
import menu from '../../../lib/menu';

class PointModel {
  constructor(coordinatesAreValid) {
    this.coordinatesAreValid = coordinatesAreValid;
  }

  static get turnRotations() {
    return {
      L: {
        name: 'Left',
        value: -1,
      },
      R: {
        name: 'Right',
        value: +1,
      },
    };
  }

  static get movementVectors() {
    return {
      N: {
        name: 'North',
        x: 0,
        y: +1,
      },
      E: {
        name: 'East',
        x: +1,
        y: 0,
      },
      S: {
        name: 'South',
        x: 0,
        y: -1,
      },
      W: {
        name: 'West',
        x: -1,
        y: 0,
      },
    };
  }

  get coordinates() {
    return this._coordinates;
  }

  set coordinates(coordinates) {
    this._coordinates = PointModel.createCoordinates(coordinates, this.coordinatesAreValid);
  }

  static createCoordinates(coordinates, coordinatesAreValid, facing = null) {
    if (!coordinatesAreValid(coordinates)) {
      const vector = facing ? 'be placed at' : `go ${PointModel.movementVectors[facing].name} to`;
      const message = `I'm sorry. I can't ${vector}: (${coordinates.x},${coordinates.y}). I'll fall off.`;
      const data = {
        coordinates,
        facing,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    return coordinates;
  }

  get coordinatesAreValid() {
    return this._coordinatesAreValid;
  }

  set coordinatesAreValid(coordinatesAreValid) {
    this._coordinatesAreValid = PointModel.createCoordinatesAreValid(coordinatesAreValid);
  }

  static createCoordinatesAreValid(coordinatesAreValid) {
    const paramType = getTypeOf(coordinatesAreValid);

    if (paramType === 'undefined') {
      const message = 'I need a function to assure me that I\'m at a valid coordinate.';
      const data = {
        coordinatesAreValid,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    if (paramType !== 'function') {
      const message = `I can't do that. I need to check coordinates using the "function" type, not the "${paramType}" type.`;
      const data = {
        coordinatesAreValid,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    return coordinatesAreValid;
  }

  coordinatesExist() {
    return !!this.coordinates;
  }

  get facing() {
    return this._facing;
  }

  set facing(facing) {
    this._facing = PointModel.createFacing(facing);
  }

  static createFacing(facing) {
    if (!PointModel.movementVectors[facing]) {
      const movementVectorChoices = Object.keys(PointModel.movementVectors);
      const message = `I'm sorry. I don't understand 'facing: "${facing}"'. Did you mean ${menu(movementVectorChoices)}?`;
      const data = {
        facing,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    return facing;
  }

  static createTurnRotationIncrement(rotation) {
    if (Object.keys(PointModel.turnRotations).indexOf(rotation) < 0) {
      const turnRotationChoices = Object.keys(PointModel.turnRotations);
      const message = `I'm sorry. I don't understand 'turn: "${rotation}"'. Did you mean ${menu(turnRotationChoices)}?`;
      const data = {
        rotation,
      };

      const error = new Error();
      error.message = message;
      error.data = data;

      throw error;
    }

    const rotationIncrement = PointModel.turnRotations[rotation].value;

    return rotationIncrement;
  }

  facingExists() {
    return !!this.facing;
  }

  place(coordinates, facing, loose = false) {
    if (loose) {
      this._coordinates = coordinates;
      this._facing = facing;

      return;
    }

    this.coordinates = PointModel.createCoordinates(coordinates, this.coordinatesAreValid, facing);
    this.facing = PointModel.createFacing(facing);
  }

  move() {
    this.predict('move');

    // Get the current facing.
    const movementVector = PointModel.movementVectors[this.facing];
    const nextCoordinates = PointModel.createCoordinates(
      {
        x: this.coordinates.x + movementVector.x,
        y: this.coordinates.y + movementVector.y,
      },
      this.coordinatesAreValid,
      this.facing
    );

    this.coordinates = nextCoordinates;
  }

  turn(rotation) {
    this.predict('turn');

    const facingIndexIncrement = PointModel.createTurnRotationIncrement(rotation);
    const facings = Object.keys(PointModel.movementVectors);
    const currFacingIndex = facings.indexOf(this.facing);

    let nextFacingIndex = currFacingIndex + facingIndexIncrement;

    if (nextFacingIndex < 0) {
      nextFacingIndex = facings.length - 1;
    } else if (nextFacingIndex > facings.length - 1) {
      nextFacingIndex = 0;
    }

    const nextFacing = facings[nextFacingIndex];

    this.facing = nextFacing;
  }

  predict(action) {
    let locationDoesExist = this.facingExists() && this.coordinatesExist();
    let locationIsValid = true;

    // NOTE:
    // An if-else block could be written to check the validity of the coordinates & facing.
    // However, doing so will defeat the purpose of implementing the static utility methods.
    //
    // The static utility methods already throw exceptions when something wrong happens.
    // It would be better to just reuse these exceptions under a try-catch block.
    if (locationDoesExist) {
      try {
        PointModel.createCoordinates(this.coordinates, this.coordinatesAreValid, this.facing);
        PointModel.createFacing(this.facing, this.coordinatesAreValid);
      } catch (error) {
        // If the model isn't generating exceptions with messages, then it's not recognized!
        if (error.message) {
          locationIsValid = false;
        }
      } 
    }

    // Guard clause approach, except that generating an error is the end goal.
    if (locationDoesExist && locationIsValid) {
      return;
    }

    const message = `I'm sorry. I can't ${action} yet. Please place me somewhere appropriate first.`;

    const error = new Error();
    error.message = message;

    throw error;
  }

  get props() {
    const coordinates = this.coordinates;
    const coordinatesAreValid = this.coordinatesAreValid;
    const facing = this.facing;

    return {
      coordinates,
      coordinatesAreValid,
      facing,
    };
  }

  report() {
    this.predict('report');

    const { coordinates, facing } = this.props;
    const movementVector = PointModel.movementVectors[this.facing];

    return `${coordinates.x},${coordinates.y},${movementVector.name.toUpperCase()}`;
  }
}

module.exports = PointModel;
