function coordinatesToString({ x, y }) {
  return `(${x},${y})`;
}

module.exports = {
  coordinatesToString
};
