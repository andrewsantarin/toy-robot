import { expect } from 'chai';

import * as PointUtils from './point.utils';

describe('PointUtils', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(PointUtils).to.not.be.undefined;
    });
  });
});
