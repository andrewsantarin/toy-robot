import { expect } from 'chai';

import { valueLookups } from '../../../../test/data';

import getTypeOf from '../../../lib/get-type-of';
import isClass from '../../../lib/is-class';
import menu from '../../../lib/menu';

import { dimensionsToString } from '../space/space.utils';
import SpaceModel from '../space/space.model';

import { coordinatesToString } from './point.utils';
import PointModel from './point.model';

const SPACE = {
  dimensions: {
    width: 3,
    height: 4,
  }
};

const space = new SpaceModel(SPACE.dimensions);

const ORIGIN = {
  coordinates: {
    x: 0,
    y: 0,
  },
};

const POINT = {
  coordinates: {
    x: SPACE.dimensions.width - 1,
    y: SPACE.dimensions.height - 1,
  },
  facing: 'N',
};

const INVALID_POINT = {
  coordinates: Object.keys(POINT.coordinates).reduce((lookup, key) => ({
    ...lookup,
    [key]: POINT.coordinates[key] + 1,
  }), {}),
  facing: 'X',
};

const TURN_ROTATIONS = {
  left: 'L',
  right: 'R',
};

describe('PointModel', () => {
  describe('module', () => {
    it('should exist', () => {
      expect(PointModel).to.not.be.undefined;
    });
  });

  describe('class', () => {
    describe('constructor', () => {
      it('should initialize with an external coordinate check function', () => {
        function coordinateChecker() {
          return true;
        }

        const point = new PointModel(coordinateChecker);

        expect(point).to.have.property('_coordinatesAreValid');
        expect(point.coordinatesAreValid()).to.equal(coordinateChecker());
      });

      it('should not initialize without an external coordinate check function', () => {
        const message = 'I need a function to assure me that I\'m at a valid coordinate.';
        const data = {
          coordinatesAreValid: undefined,
        };

        function createPointModel() {
          return new PointModel();
        }

        expect(createPointModel).to.throw(Error).with.property('message', message);
        expect(createPointModel).to.throw(Error).with.deep.own.property('data', data);
      });

      class CoordinateCheckerTestClass {
        constructor() {

        }
      }

      const coordinatesValidators = [
        true,
        1,
        'a',
        [],
        {},
        Date,
        new Date(),
        CoordinateCheckerTestClass,
        new CoordinateCheckerTestClass(),
        null,
      ];

      coordinatesValidators.forEach((coordinatesValidator) => {
        let type = getTypeOf(coordinatesValidator);

        it(`should not initialize when external coordinate check is a type of: ${type}`, () => {
          const message = `I can't do that. I need to check coordinates using the "function" type, not the "${type}" type.`;
          const data = {
            coordinatesAreValid: coordinatesValidator,
          };

          function createPointModel() {
            return new PointModel(coordinatesValidator);
          }

          expect(createPointModel).to.throw(Error).with.property('message', message);
          expect(createPointModel).to.throw(Error).with.deep.own.property('data', data);
        });
      });
    });

    describe('function::instance', () => {
      describe('place', () => {
        describe(`on Space (${dimensionsToString(SPACE.dimensions)})`, () => {
          const point = new PointModel(space.coordinatesInBounds.bind(space));

          it(`should set instance at ${coordinatesToString(POINT.coordinates)}, facing "${POINT.facing}": valid`, () => {
            function place() {
              point.place(POINT.coordinates, POINT.facing);

              return point.props;
            }

            expect(place).to.not.throw(Error);
            expect(place().coordinates).to.deep.equal(POINT.coordinates);
            expect(place().facing).to.equal(POINT.facing);
          });

          const MISSING_COORDINATE_LOOKUPS = [
            INVALID_POINT.coordinates,
            { x: POINT.coordinates.x },
            { y: POINT.coordinates.y },
          ];

          (MISSING_COORDINATE_LOOKUPS).forEach((coordinates) => {
            it(`should set instance at ${coordinatesToString(coordinates)}, facing "${POINT.facing}": invalid coordinates`, () => {
              const vector = POINT.facing ? 'be placed at' : `go ${PointModel.movementVectors[facing].name} to`;
              const message = [
                `I'm sorry. I can't ${vector}: (${INVALID_POINT.coordinates.x},${INVALID_POINT.coordinates.y}).`,
                `I'll fall off.`,
              ].join(' ');
              const data = {
                coordinates: INVALID_POINT.coordinates,
                facing: POINT.facing,
              };

              function place() {
                point.place(INVALID_POINT.coordinates, POINT.facing);

                return point.props;
              }

              expect(place).to.throw(Error).with.property('message', message);
              expect(place).to.throw(Error).with.deep.own.property('data', data);
            });
          });

          it(`should set instance at ${coordinatesToString(POINT.coordinates)}, facing "${INVALID_POINT.facing}": invalid facing`, () => {
            const message = [
              `I'm sorry. I don't understand 'facing: "${INVALID_POINT.facing}"'.`,
              `Did you mean ${menu(Object.keys(PointModel.movementVectors))}?`,
            ].join(' ');
            const data = {
              facing: INVALID_POINT.facing,
            };

            function place() {
              point.place(POINT.coordinates, INVALID_POINT.facing);

              return point.props;
            }

            expect(place).to.throw(Error).with.property('message', message);
            expect(place).to.throw(Error).with.deep.own.property('data', data);
          });

          it(`should set instance at ${coordinatesToString(INVALID_POINT.coordinates)}, facing "${POINT.facing}": overriding validation`, () => {
            function place() {
              point.place(INVALID_POINT.coordinates, POINT.facing, true);

              return point.props;
            }

            expect(place).to.not.throw(Error);
            expect(place().coordinates).to.deep.equal(INVALID_POINT.coordinates);
            expect(place().facing).to.equal(POINT.facing);
          });

          it(`should set instance at ${coordinatesToString(POINT.coordinates)}, facing "${INVALID_POINT.facing}": overriding validation`, () => {
            function place() {
              point.place(INVALID_POINT.coordinates, POINT.facing, true);

              return point.props;
            }

            expect(place).to.not.throw(Error);
            expect(place().coordinates).to.deep.equal(INVALID_POINT.coordinates);
            expect(place().facing).to.equal(POINT.facing);
          });
        });
      });

      describe('move', () => {
        describe(`at corner coordinate ${coordinatesToString(POINT.coordinates)}`, () => {
          describe('invalid movement', () => {
            const point = new PointModel(space.coordinatesInBounds.bind(space));

            it('should not set coordinates when not placed', () => {
              const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

              function move() {
                point.move();

                return point.coordinates;
              }

              expect(move).to.throw(Error).with.property('message', message);
            });

            it('should not set coordinates when placed with invalid facing', () => {
              point.place(POINT.coordinates, INVALID_POINT.facing, true);

              const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

              function move() {
                point.move();

                return point.coordinates;
              }

              expect(move).to.throw(Error).with.property('message', message);
            });

            const MISSING_COORDINATE_LOOKUPS = [
              INVALID_POINT.coordinates,
              { x: POINT.coordinates.x },
              { y: POINT.coordinates.y },
            ];

            (MISSING_COORDINATE_LOOKUPS).forEach((coordinates) => {
              it(`should not set coordinates when placed with invalid coordinates at ${coordinatesToString(coordinates)}`, () => {
                point.place(coordinates, POINT.facing, true);

                const message = `I'm sorry. I can't move yet. Please place me somewhere appropriate first.`;

                function move() {
                  point.move();

                  return point.coordinates;
                }

                expect(move).to.throw(Error).with.property('message', message);
              });
            });
          });

          describe('valid movement', () => {
            const point = new PointModel(space.coordinatesInBounds.bind(space));
            const DIRECTIONS = ['N', 'E', 'S', 'W'];

            it('should have 4 directions arranged clockwise starting from "N"', () => {
              expect(DIRECTIONS).to.deep.equal(Object.keys(PointModel.movementVectors));
            });

            const caseName = [
              'should move 1 unit in every direction',
              'in a clockwise circle starting from "N"',
              'using .place() instead of .turn(),',
              `returning back to ${coordinatesToString(ORIGIN.coordinates)}`,
            ].join(' ');

            describe(caseName, () => {
              let c = ORIGIN.coordinates;

              const COORDINATES_LOOKUPS = (DIRECTIONS).map((direction) => {
                c = Object.keys(c).reduce((lookup, key) => ({
                  ...lookup,
                  [key]: c[key] + PointModel.movementVectors[direction][key],
                }), {});

                return {
                  coordinates: c,
                  facing: direction,
                };
              });

              // Reuse.
              c = ORIGIN.coordinates;

              (COORDINATES_LOOKUPS).forEach((lookup) => {
                const { coordinates, facing } = lookup;
                const movementVector = PointModel.movementVectors[facing];

                it(`set coordinates to ${coordinatesToString(coordinates)}, facing: "${facing}"`, () => {
                  expect(true).to.not.be.undefined;

                  point.place(c, facing);

                  function move() {
                    point.move();

                    return point.coordinates;
                  }

                  expect(move).to.not.throw(Error);
                  expect(point.coordinates).to.deep.equal(coordinates);
                  expect(point.facing).to.equal(facing);

                  c = Object.keys(c).reduce((lookup, key) => ({
                    ...lookup,
                    [key]: c[key] + movementVector[key],
                  }), {});
                });
              });
            });
          });
        });

        const COORDINATES_LOOKUPS = [
          {
            coordinates: ORIGIN.coordinates,
            name: 'Origin',
            directions: {
              valid: [ 'N', 'E' ],
              invalid: [ 'S', 'W' ],
            },
          },
          {
            coordinates: POINT.coordinates,
            name: 'Point',
            directions: {
              valid: [ 'S', 'W' ],
              invalid: [ 'N', 'E' ],
            },
          },
        ];

        (COORDINATES_LOOKUPS).forEach(({ coordinates, name, directions }) => {
          describe(`at coordinate ${name} ${coordinatesToString(coordinates)}`, () => {
            const point = new PointModel(space.coordinatesInBounds.bind(space));

            function caseName(coordinates, facing, positive = true) {
              return `should be ${positive ? 'able' : 'unable'} to move "${facing}" to ${coordinatesToString(coordinates)}`;
            }

            describe('valid movement', () => {
              directions.valid.forEach((facing) => {
                const movementVector = PointModel.movementVectors[facing];
                const expectedCoordinates = Object.keys(coordinates).reduce((lookup, key) => ({
                  ...lookup,
                  [key]: coordinates[key] + movementVector[key],
                }), {});

                it(caseName(expectedCoordinates, facing), () => {
                  point.place(coordinates, facing);

                  function move() {
                    point.move();

                    return point.coordinates;
                  }

                  expect(move).to.not.throw(Error);
                  expect(point.coordinates).to.deep.equal(expectedCoordinates);
                  expect(point.facing).to.equal(facing);
                });
              });
            });

            describe('invalid movement', () => {
              directions.invalid.forEach((facing) => {
                const movementVector = PointModel.movementVectors[facing];
                const expectedCoordinates = Object.keys(coordinates).reduce((lookup, key) => ({
                  ...lookup,
                  [key]: coordinates[key] + movementVector[key],
                }), {});

                it(caseName(expectedCoordinates, facing, false), () => {
                  point.place(coordinates, facing);

                  const vector = facing ? 'be placed at' : `go ${movementVector.name} to`;
                  const message = `I'm sorry. I can't ${vector}: (${expectedCoordinates.x},${expectedCoordinates.y}). I'll fall off.`;
                  const data = {
                    coordinates: expectedCoordinates,
                    facing,
                  };

                  function move() {
                    point.move();

                    return point.coordinates;
                  }

                  expect(move).to.throw(Error).with.property('message', message);
                  expect(move).to.throw(Error).with.own.deep.property('data', data);
                });
              });
            });
          });
        });
      });

      describe('turn', () => {
        describe(`from direction "${POINT.facing}"`, () => {
          describe('invalid rotation', () => {
            const point = new PointModel(space.coordinatesInBounds.bind(space));

            it('should not set direction when not placed', () => {
              const rotation = TURN_ROTATIONS.left;
              const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

              function turn() {
                point.turn(rotation);

                return point.facing;
              }

              expect(turn).to.throw(Error).with.property('message', message);
            });

            it('should not set direction when placed with invalid facing', () => {
              point.place(POINT.coordinates, INVALID_POINT.facing, true);

              const rotation = TURN_ROTATIONS.left;
              const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

              function turn() {
                point.turn(rotation);

                return point.facing;
              }

              expect(turn).to.throw(Error).with.property('message', message);
            });

            it('should not set direction when placed with invalid coordinates', () => {
              point.place(INVALID_POINT.coordinates, POINT.facing, true);

              const rotation = TURN_ROTATIONS.left;
              const message = `I'm sorry. I can't turn yet. Please place me somewhere appropriate first.`;

              function turn() {
                point.turn(rotation);

                return point.facing;
              }

              expect(turn).to.throw(Error).with.property('message', message);
            });

            valueLookups.forEach((valueLookup) => {
              const { name, value } = valueLookup;

              it(`should not set direction with invalid facing: ${name}`, () => {
                point.place(POINT.coordinates, POINT.facing);

                const rotation = value;

                const turnRotationChoices = Object.keys(PointModel.turnRotations);
                const message = `I'm sorry. I don't understand 'turn: "${rotation}"'. Did you mean ${menu(turnRotationChoices)}?`;
                const data = {
                  rotation,
                };

                function turn() {
                  point.turn(rotation);

                  return point.facing;
                }

                expect(turn).to.throw(Error).with.property('message', message);
                expect(turn).to.throw(Error).with.deep.own.property('data', data);
              });
            });
          });

          describe('rotating once', () => {
            const point = new PointModel(space.coordinatesInBounds.bind(space));

            const ROTATION_LOOKUPS = [
              {
                rotation: TURN_ROTATIONS.left,
                expected: 'W',
              },
              {
                rotation: TURN_ROTATIONS.right,
                expected: 'E',
              },
            ];

            (ROTATION_LOOKUPS).forEach((lookup) => {
              const { expected, rotation } = lookup;

              it(`should set direction to "${expected}": turn ${PointModel.turnRotations[rotation].name.toLowerCase()}`, () => {
                point.place(POINT.coordinates, POINT.facing);

                function turn() {
                  point.turn(rotation);

                  return point.facing;
                }

                expect(turn()).to.equal(expected);
              });
            });
          });

          const SPINS = {
            clockwise: {
              directions: [ 'E', 'S', 'W', 'N' ],
              turnRotation: 'R',
            },
            anticlockwise: {
              directions: [ 'W', 'S', 'E', 'N' ],
              turnRotation: 'L',
            },
          };

          Object.keys(SPINS).forEach((key) => {
            describe(`spinning ${key}`, () => {
              const point = new PointModel(space.coordinatesInBounds.bind(space));
              const spinning = SPINS[key];
              const { directions, turnRotation } = spinning;

              point.place(POINT.coordinates, POINT.facing);

              directions.forEach((direction) => {
                it(`should set direction to "${direction}"`, () => {
                  function turn() {
                    point.turn(turnRotation);

                    return point.facing;
                  }

                  expect(turn()).to.equal(direction);
                });
              });
            });
          });
        });
      });
    });
  });
});
