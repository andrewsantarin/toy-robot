import React, { Component } from 'react';
import blessed from 'neo-blessed';
import { createBlessedRenderer } from 'react-blessed';

import App from './app/App';

// Create screen.
const screen = blessed.screen({
  autoPadding: true,
  smartCSR: true,
  title: 'toy-robot/Toy Robot Simulator CLI',
});

// Add a way to quit the program.
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

const render = createBlessedRenderer(blessed);

// Render the React app using the screen.
const component = render(<App />, screen);
