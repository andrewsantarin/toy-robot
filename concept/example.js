const GRID = {
  dimensions: {
    width: 5,
    height: 5,
  },
};

let grid = new Grid(GRID.dimensions);
let robot = new Robot(grid.coordinatesInBounds.bind(grid));

const START_LOCATION = {
  coordinates: {
    x: 0,
    y: 0,
  },
  facing: 'E',
};

robot.place(START_LOCATION.coordinates, START_LOCATION.facing);
