class Grid {
  constructor(dimensions = Grid.defaultProps.dimensions) {
    this._matrix = Grid.createMatrix(dimensions);
  }

  get matrix() {
    return this._matrix;
  }

  set matrix(dimensions = Grid.defaultProps.dimensions) {
    this._matrix = Grid.createMatrix(dimensions);
  }

  static get defaultProps() {
    return {
      dimensions: {
        width: 0,
        height: 0,
      },
    };
  }

  static createMatrix({ width, height }) {
    if (!width || !height) {
      const message = 'The grid won\'t work with no width or no height.';

      throw {
        message,
        data: {
          width,
          height,
        },
      };
    }

    return range(0, height).map((y) => range(0, width).map((x) => ({
      x,
      y,
    })));
  }

  coordinatesInBounds({ x, y }) {
    return !!this._matrix[y] && !!this._matrix[y][x];
  }

  getProps() {
    return {
      matrix: this._matrix,
    };
  }

  report() {
    const { matrix } = this.getProps();

    const matrixStr = matrix.map((points) => points.map((point) => `${point.x},${point.y}`).join('  ')).join('\n\n');

    return matrixStr;
  }
}
