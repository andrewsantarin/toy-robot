class Robot {
  constructor(coordinatesAreValid = Robot.defaultProps.coordinatesAreValid) {
    this._coordinatesAreValid = coordinatesAreValid;
  }

  static get defaultProps() {
    return {
      coordinatesAreValid: () => false,
      coordinates: {
        x: 0,
        y: 0,
      },
      facing: 'N',
    };
  }

  static get turnRotations() {
    return {
      L: {
        name: 'Left',
        value: -1,
      },
      R: {
        name: 'Right',
        value: +1,
      },
    };
  }

  static get movementVectors() {
    return {
      N: {
        name: 'North',
        x: 0,
        y: +1,
      },
      E: {
        name: 'East',
        x: +1,
        y: 0,
      },
      S: {
        name: 'South',
        x: 0,
        y: -1,
      },
      W: {
        name: 'West',
        x: -1,
        y: 0,
      },
    };
  }

  get coordinates() {
    return this._coordinates;
  }

  set coordinates(coordinates) {
    this._coordinates = Robot.createCoordinates(coordinates, this._coordinatesAreValid);
  }

  static createCoordinates({ x, y }, coordinatesAreValid, facing = null) {
    const coordinates = { x, y };

    if (!coordinatesAreValid(coordinates)) {
      const vector = facing ? 'be placed at' : `go ${Robot.movementVectors[facing].name} to`;
      const message = `I'm sorry. I can't ${vector}: (${coordinates.x},${coordinates.y}). I'll fall off.`;

      throw {
        message,
        data: {
          coordinates,
          coordinatesAreValid,
          facing,
        },
      };
    }

    return coordinates;
  }

  coordinatesExist() {
    return !!this._coordinates;
  } 

  place(
    coordinates = Robot.defaultProps.coordinates,
    facing = Robot.defaultProps.facing,
    loose = false,
  ) {
    if (loose) {
      this._coordinates = coordinates;
      this._facing = facing;

      return;
    }

    this._coordinates = Robot.createCoordinates(coordinates, this._coordinatesAreValid, facing);
    this._facing = Robot.createFacing(facing);
  }

  get facing() {
    return this._facing;
  }

  set facing(facing) {
    this._facing = Robot.createFacing(facing);
  }

  static createFacing(facing) {
    if (!Robot.movementVectors[facing]) {
      const movementVectorChoices = Object.keys(Robot.movementVectors);
      const message = `I'm sorry. I don't understand 'facing: "${facing}"'. Did you mean ${menu(movementVectorChoices)}?`;

      throw {
        message,
        data: {
          facing,
        },
      };
    }

    return facing;
  }

  static createTurnRotationIncrement(rotation) {
    if (Object.keys(Robot.turnRotations).indexOf(rotation) < 0) {
      const turnRotationChoices = Object.keys(Robot.turnRotations);
      const message = `I'm sorry. I don't understand 'turn: "${rotation}"'. Did you mean ${menu(turnRotationChoices)}?`;

      throw {
        message,
        data: {
          rotation,
        },
      };
    }

    const rotationIncrement = Robot.turnRotations[rotation].value;

    return rotationIncrement;
  }

  move() {
    if (!this.coordinatesExist()) {
      const message = `I'm sorry. I can't move yet. Please place me somewhere first.`;

      throw {
        message,
      };
    }

    this._facing = Robot.createFacing(this._facing);

    // Get the current facing.
    const movementVector = Robot.movementVectors[this._facing];
    const nextCoordinates = Robot.createCoordinates(
      {
        x: this._coordinates.x + movementVector.x,
        y: this._coordinates.y + movementVector.y,
      },
      this._coordinatesAreValid,
      this._facing
    );

    this._coordinates = nextCoordinates;
  }

  turn(rotation) {
    const facingIndexIncrement = Robot.createTurnRotationIncrement(rotation);
    const facings = Object.keys(Robot.movementVectors);
    const currFacingIndex = facings.indexOf(this._facing);

    let nextFacingIndex = currFacingIndex + facingIndexIncrement;

    if (nextFacingIndex < 0) {
      nextFacingIndex = facings.length - 1;
    } else if (nextFacingIndex > facings.length - 1) {
      nextFacingIndex = 0;
    }

    const nextFacing = facings[nextFacingIndex];

    this._facing = nextFacing;
  }

  getProps() {
    if (!this.coordinatesExist()) {
      const message = `I'm here, but I need to be placed somewhere first.`;

      throw {
        message,
      };
    }

    const coordinates = this._coordinates;
    const facing = Robot.movementVectors[this._facing];

    const {
      _coordinates: coordinates,
      _coordinatesAreValid: coordinatesAreValid,
      _facing: facing,
    } = this;

    return {
      coordinates,
      coordinatesAreValid,
      facing,
    };
  }

  report() {
    const { coordinates, facing: _facing } = this.getProps();

    const facing = Robot.movementVectors[_facing];

    return `${coordinates.x},${coordinates.y},${facing.name.toUpperCase()}`;
  }
}
