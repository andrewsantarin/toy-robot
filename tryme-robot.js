import TabletopModel from './src/app/tabletop/tabletop.model';
import RobotModel from './src/app/robot/robot.model';
import { commandLookups } from './test/data';

const TABLETOP = {
  dimensions: {
    width: 5,
    height: 5,
  },
};

let tabletop = new TabletopModel(TABLETOP.dimensions);
let robot = new RobotModel(tabletop.coordinatesInBounds.bind(tabletop));

commandLookups.forEach(({ commands, expected }) => {
  console.log('Input:');

  commands.forEach((command) => {
    const result = robot.readCommand(command);
    console.log(`> ${command}`);

    if (result) {
      console.log('');
      console.log('Output:');
      console.log(result);
      console.log('');
    }
  });

  console.log('========');
  console.log('')
});
