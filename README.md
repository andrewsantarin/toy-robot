# toy-robot
Toy robot simulator CLI in Node.js, React.js and the `react-blessed` library.

## What the code does
It creates a scenario where you can give movement commands to a robot on a tabletop of a given area. The tabletop is clean of obstacles and the robot can move about in these 4 basic directions: north, south, east, west. Each movement command puts the robot forward by one unit. The robot will not respond to any commands that will let it fall off the table.

Alternatively, see the [PROBLEM.md](https://bitbucket.org/andrewsantarin/toy-robot/src/master/PROBLEM.md) file.

## System requirements
This project assumes only the most minimum number of tools will be needed for running the application.

You'll need [Node.js](https://nodejs.org) & [npm](https://www.npmjs.com) (which should already be installed by Node.js) installed into your machine. The **package.json** file included in this repository will need you to use the latest version of Node.js, which should be at least v10 at this time of writing. [Yarn](https://yarnpkg.com) isn't required, but you're free to replace npm with it.

## Why this infrastructure?
### I've been down that road before
The majority of my software development career these days is writing apps on JavaScript (ECMAScript 6) and specifically, the React.js view library, which this project is also written in. Pure Node.js is a bit more alien to me (given my frontender background), but with so many resources out there, I figured, "Why not?" and have a go at it.

### Challenges
Unit tests & Test Driven Development is something I admittedly (and regrettably) don't practice a lot as a whole, but this challenge has been a good learning experience. That, and the need to make a CLI, which, coming from a Web development background, is quite different altogether.

## Folder structure
The source code is organized according to their business domains (e.g. `/customer/customer.model.js`, `/customer/components/Customer.jsx`, `/order/order.util.js`) rather than their technical nature (e.g. `/models/customer.js`, `/models/order.js`, `/containers/Customer.jsx`), which makes it arbitrarily easier to look up code by their common business requirements. That being said, it is mostly [Domain-Driven Design](https://medium.com/the-coding-matrix/ddd-101-the-5-minute-tour-7a3037cf53b8) rather than Nature-Driven. If you write in Django, this concept will ring a few bells. Overall, not a new concept. I've seen this posted in 2015: http://marmelab.com/blog/2015/12/17/react-directory-structure.html, so this idea goes way back.

## Usage
Run these commands in your preferred CLI:
```bash
git clone https://andrewsantarin@bitbucket.org/andrewsantarin/toy-robot.git
npm install
npm start
```

**Note**: This project is version-controlled in Bitbucket. If you'd rather use GUI, there's a section for you, "**Cloning this repository via SourceTree**" where you can use SourceTree, Bitbucket's client.

## Testing
Run this command in your preferred CLI:
```
npm test
```

You can also check out the sample data at work, which has been outlined in the [PROBLEM.md](https://bitbucket.org/andrewsantarin/toy-robot/src/master/PROBLEM.md) file.
```
npm run tryme
```

## Tracking
Issues can be filed [through the usual means](https://bitbucket.org/andrewsantarin/toy-robot/issues?status=new&status=open). For a more succint point of view of progress, you can visit [this integrated Trello board](https://trello.com/b/xV7iOlan/toy-robot) instead.

## Cloning this repository via SourceTree

Use these steps to clone from SourceTree, Bitbucket's client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You'll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you'd like to and then click **Clone**.
4. Open the directory you just created to see your repository's files.
